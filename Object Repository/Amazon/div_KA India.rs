<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_KA India</name>
   <tag></tag>
   <elementGuidId>b9b7e41e-91cd-43e2-a1f2-3a0086a0f544</elementGuidId>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home-search&quot;)/div[1]/form[1]/div[@class=&quot;location-search-bar&quot;]/span[@class=&quot;searchbox location-search&quot;]/span[@class=&quot;twitter-typeahead&quot;]/div[@class=&quot;tt-menu tt-open&quot;]/div[@class=&quot;tt-dataset tt-dataset-location-results&quot;]/div[@class=&quot;tt-suggestion tt-selectable&quot;]/div[@class=&quot;suggestion-link&quot;]/div[@class=&quot;details&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>KA, India</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>KA, India</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>details</value>
   </webElementProperties>
</WebElementEntity>
