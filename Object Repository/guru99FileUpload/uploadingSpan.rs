<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>uploadingSpan</name>
   <tag></tag>
   <elementGuidId>494dc319-b01f-4400-bc64-e970c6493736</elementGuidId>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>ul[@class=‘qq-upload-list’]//span[contains(@class,’qq-file-uploading’)]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/guru99FileUpload/formIframe</value>
   </webElementProperties>
</WebElementEntity>
