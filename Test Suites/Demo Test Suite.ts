<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Demo Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2017-09-03T21:54:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ec7d180c-a59d-48d8-aa28-977fc95c1135</testSuiteGuid>
   <testCaseLink>
      <guid>e808c172-6e29-4877-8bec-751adfd6c43f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Main Test Cases/VerifyAmazonCareers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec43f5e5-dbd8-4328-b2d6-1f440776627d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Main Test Cases/verifyUploadFiles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>160e74b2-9d2f-4aa2-924e-47e9c6889586</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Main Test Cases/dataDrivenTestingWithParameterization</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4f613714-0a45-4b8b-8e8e-77744f36fea4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>705509c1-6d74-42c9-87b0-5055473c2ded</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0586cc3d-e3a9-4065-b7d6-713f769c5b74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Main Test Cases/LoginToAmazon</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
