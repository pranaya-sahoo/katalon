import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://www.amazon.in/')

WebUI.click(findTestObject('Amazon/a_Careers'))

WebUI.setText(findTestObject('Amazon/input_base_query'), 'Java')

WebUI.setText(findTestObject('Amazon/input_location-typeahead'), 'Bengaluru, KA, India')

WebUI.click(findTestObject('Amazon/span_button-icon'))

WebUI.closeBrowser()

WebUI.openBrowser('')

WebUI.navigateToUrl('http://www.amazon.in/')

WebUI.click(findTestObject('Amazon/a_Careers (1)'))

WebUI.setText(findTestObject('Amazon/input_base_query (1)'), 'python')

WebUI.click(findTestObject('Amazon/div_KA India'))

WebUI.click(findTestObject('Amazon/span_button-icon (1)'))

WebUI.setText(findTestObject('Amazon/input_location-typeahead (1)'), 'Bengaluru, KA, India')

WebUI.closeBrowser()

