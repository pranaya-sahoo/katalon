import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')
WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.G_AmazonURL)

WebUI.scrollToElement(findTestObject('Amazon/Carrers Page/CareersLink/a_Careers'), 0)

WebUI.click(findTestObject('Amazon/Carrers Page/CareersLink/a_Careers'))



ArrayList<String> cities = new ArrayList<String>();
cities.add("Bangalore");
cities.add("California");

WebUI.setText(findTestObject('Object Repository/Amazon/Carrers Page/SearchParameters/input_base_query')
,"Selenium")

HashMap<String,Integer> openPositions = new HashMap<String,Integer>();

String positions = null;
String openPositionsResult = null;

for(int i = 0; i < cities.size();i++){
	
	WebUI.setText( findTestObject('Object Repository/Amazon/Carrers Page/SearchParameters/input_location-typeahead')  ,
		 cities.get(i))
	WebUI.waitForElementClickable(findTestObject('Amazon/Carrers Page/JobSearchResultPage/button_search-button'), 60)
	
	WebUI.click(findTestObject('Amazon/Carrers Page/JobSearchResultPage/button_search-button'))
	WebUI.waitForElementVisible(findTestObject('Amazon/Carrers Page/JobSearchResultPage/OpenPositions'),60)
	//Thread.sleep(3000);
	openPositionsResult = 
	WebUI.getText(findTestObject('Amazon/Carrers Page/JobSearchResultPage/OpenPositions'));

	positions = openPositionsResult.split("of")[1].trim().split(" ")[0].trim();
	openPositions.put(cities.get(i), Integer.parseInt(positions))
	
}

	// Find out the city with highest open position
	LinkedHashSet<String> city = openPositions.keySet();	
	ArrayList<String> al = new ArrayList<String>(city);	
	if(openPositions.get(al.get(0))>openPositions.get(al.get(1))){
		println(al.get(0)+" has higher number of open positions in comparison to "+al.get(1))
	}else{
		println(al.get(1)+" has higher number of open positions in comparison to "+al.get(0))
	}
	WebUI.closeBrowser();

