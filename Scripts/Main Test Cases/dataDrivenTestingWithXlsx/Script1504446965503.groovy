import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.InternalData

InternalData data = findTestData("TestData")


for (def index : (0..data.getRowNumbers() - 1)){

WebUI.openBrowser("")
WebUI.maximizeWindow();
WebUI.navigateToUrl("http://demoaut.katalon.com/profile.php")

WebUI.setText(findTestObject('Object Repository/KatalonBookAppointment/input_username'), 
	data.internallyGetValue("UserName", index), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/KatalonBookAppointment/input_password'), 
	data.internallyGetValue("Password", index), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/KatalonBookAppointment/button_Login'))

WebUI.click(findTestObject('Object Repository/KatalonBookAppointment/a_menu-toggle'))

WebUI.click(findTestObject('Object Repository/KatalonBookAppointment/a_Logout'))

}